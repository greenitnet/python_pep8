from python:alpine3.19

RUN mkdir /code
ADD . /code/

WORKDIR /code
RUN pip install -r requirements.txt
